<?php
	$path = '../../../';
	$description = 'México gana a Alemania 1 - 0 el resultado final';
	$keywords = 'La selección mexicana gana su primer partido en la copa Mundial de Rusia 2018';
	include($path . 'v/head.phtml');
?>
<body class="">
	<noscript>
		<div class="no-js-menu clearfix">
			<ul>
				<li><i class="fa fa-home"></i><a href="http://armandomariscal.com">Inicio</a></li>
				<li><i class="fa fa-twitter"></i><a href="http://twitter.com/armandomariscal">Twitter</a></li>
				<li><i class="fa fa-github"></i><a href="http://github.com/armandomariscal">Github</a></li>
				<li><i class="fa fa-pencil"></i><a href="http://armandomariscal.blogspot.com">Dibujos</a></li>
			</ul>
		</div>
	</noscript>
	<main class="container left-container">
		<div class="row">

					<div id="menu-target">
				<ul>
					<li><i class="fa fa-home"></i><a href="http://armandomariscal.com">Inicio</a></li>
					<li><i class="fa fa-twitter"></i><a href="http://twitter.com/armandomariscal">Twitter</a></li>
					<li><i class="fa fa-github"></i><a href="http://github.com/armandomariscal">Github</a></li>
					<li><i class="fa fa-pencil"></i><a href="http://armandomariscal.blogspot.com">Dibujos</a></li>
				</ul>
			</div>
			<section class="sidebar col-md-5 col-sm-12" style="background-image: url(<?= $path ?>img/feature-sea.jpg);">
				<span class="menu-trigger animated fadeInDown">
					<span class="bar"></span>
					<span class="bar"></span>
					<span class="bar"></span>
				</span>
				<div class="site-info">
					<div class="primary-info">
						<h1>Armando Mariscal</h1>
						<p></p>
					</div>
				</div>
			</section><!-- end sidebar -->
			<div class="col-md-7 col-sm-12 col-md-offset-5 main-content">
				<a href="<?= $path ?>">
					<h2 class="favorites">Armando Mariscal / 2018 / 06 / 15</h2>
				</a>
				<div class="posts-block animated fadeIn ">
					<article class="post">
						<div class="post-preview">
							<h2>Argentina empata contra Islandia 1 - 1 el resultado final</h2>
							<h3>La selección argentina empata su primer partido en la copa mundial <strong>Rusia 2018</strong></h3>
							<img src="img/argentina-empata-su primer-partido-en-la-copa mundial-2018.jpg" alt="Messi falla penal en su primer partido del Mundial Rusia 2018">
							<p>
								Es la primera participación en un mundial para Islandia y vaya que no se dejo intimidar por el bicampeón albiceleste.
							</p>
							<h3>
								¿Cuántas veces a ganado Argentina un Mundial?
							<h3>
							<p>
								La Selección de fútbol de Argentina a ganado en 2 ocasiones:
							<ul>
								<li>Copa del Mundo 1986</li>
								<li>Copa del Mundo 1978</li>
							</ul>
							</p>
							<p class="meta">
								<span>Por: Armando Mariscal</span>
								<br>
								<span>16 de Junio del 2018</span>
							</p>
						</div>
					</article>
					
					<article class="post">
						<div class="post-preview">
							<h2><a href="#">Publicidad</a></h2>
							<style type="text/css">
							.adslot_1 { display:inline-block; width: 320px; height: 50px; }
							@media (max-width: 400px) { .adslot_1 { display: none; } }
							@media (min-width:500px) { .adslot_1 { width: 468px; height: 60px; } }
							@media (min-width:800px) { .adslot_1 { width: 728px; height: 90px; } }
						</style>
						<ins class="adsbygoogle adslot_1"
						data-ad-client="ca-pub-9467923145415009"
						data-ad-slot="3118449251"></ins>
						<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
					</div>
				</article>
			</div>
			<footer class="split-footer">
<a href="www.armandomariscal.com">www.armandomariscal.com</a>
<i class="link-spacer"></i>
2018
</footer>
</div><!-- main content -->
</div> <!--/row -->
</main> <!-- /container -->
<?php
	include($path . 'v/footer.phtml');
?>