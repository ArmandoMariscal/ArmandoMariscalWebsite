<?php
	$path = '../../../';
	$description = '¿Cómo se llama la canción del Mundial 2018?';
	$keywords = 'Canción del Mundial 2018, Preguntas Frecuentes del Mundial de Rusia 2018';
	include($path . 'v/head.phtml');
?>
<body class="">
	<noscript>
		<div class="no-js-menu clearfix">
			<ul>
				<li><i class="fa fa-home"></i><a href="http://armandomariscal.com">Inicio</a></li>
				<li><i class="fa fa-twitter"></i><a href="http://twitter.com/armandomariscal">Twitter</a></li>
				<li><i class="fa fa-github"></i><a href="http://github.com/armandomariscal">Github</a></li>
				<li><i class="fa fa-pencil"></i><a href="http://armandomariscal.blogspot.com">Dibujos</a></li>
			</ul>
		</div>
	</noscript>
	<main class="container left-container">
		<div class="row">

					<div id="menu-target">
				<ul>
					<li><i class="fa fa-home"></i><a href="http://armandomariscal.com">Inicio</a></li>
					<li><i class="fa fa-twitter"></i><a href="http://twitter.com/armandomariscal">Twitter</a></li>
					<li><i class="fa fa-github"></i><a href="http://github.com/armandomariscal">Github</a></li>
					<li><i class="fa fa-pencil"></i><a href="http://armandomariscal.blogspot.com">Dibujos</a></li>
				</ul>
			</div>
			<section class="sidebar col-md-5 col-sm-12" style="background-image: url(<?= $path ?>img/feature-sea.jpg);">
				<span class="menu-trigger animated fadeInDown">
					<span class="bar"></span>
					<span class="bar"></span>
					<span class="bar"></span>
				</span>
				<div class="site-info">
					<div class="primary-info">
						<h1>Armando Mariscal</h1>
						<p></p>
					</div>
				</div>
			</section><!-- end sidebar -->
			<div class="col-md-12 col-sm-12 col-md-offset-5 main-content">
				<a href="<?= $path ?>">
					<h2 class="favorites">Armando Mariscal / 2018 / 06 / 15</h2>
				</a>
				<div class="posts-block animated fadeIn ">
					<article class="post">
						<div class="post-preview">
							<h2>¿Cómo se llama la canción del Mundial 2018?</h2>
							<h3>El nombre es Live it up, y es la canción oficial del Mundial de Rusia 2018</h3>
							<p>
								Interpretada por Nicky Jam feat. Will Smith & Era Istrefi
							</p>
							<p>
								Ahora que inicio el mundial el día de ayer 14 de Junio del 2018, me pregunto. <br>	
								<strong>	¿Cuál será el tema para la Copa Mundial de Fútbol de 2026?</strong>
							</p>
							<p>	
								<iframe width="560" height="315" src="https://www.youtube.com/embed/V15BYnSr0P8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</p>
							<p class="meta">
								<span>Armando Mariscal</span>
								<span><?= date('M') . ' ' . date('d') . ', ' . date('Y') ?></span>
							</p>
						</div>
					</article>
					
					<article class="post">
						<div class="post-preview">
							<h2><a href="#">Publicidad</a></h2>
							<style type="text/css">
							.adslot_1 { display:inline-block; width: 320px; height: 50px; }
							@media (max-width: 400px) { .adslot_1 { display: none; } }
							@media (min-width:500px) { .adslot_1 { width: 468px; height: 60px; } }
							@media (min-width:800px) { .adslot_1 { width: 728px; height: 90px; } }
						</style>
						<ins class="adsbygoogle adslot_1"
						data-ad-client="ca-pub-9467923145415009"
						data-ad-slot="3118449251"></ins>
						<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
					</div>
				</article>
			</div>
			<footer class="split-footer">
<a href="www.armandomariscal.com">www.armandomariscal.com</a>
<i class="link-spacer"></i>
2018
</footer>
</div><!-- main content -->
</div> <!--/row -->
</main> <!-- /container -->
<?php
	include($path . 'v/footer.phtml');
?>