<?php
	$path = '../../../';
	$description = 'Argentina pierde contra Croacia en fase de grupos Rusia 2018';
	$keywords = 'Argentina pierde contra Croacia en fase de grupos Rusia 2018';
	include($path . 'v/head.phtml');
	include($path . 'v/menu.phtml');
?>
<div class="posts-block animated fadeIn ">
	<article class="post">
		<div class="post-preview">
			<h2>Argentina pierde contra Croacia en fase de grupos Rusia 2018</h2>
			<h3>3 - 0 resultado final</h3>
			<img src="img/argentina-pierde-contra-croacia-en-su-segundo-partido-del-mundial-2018.jpg" alt="argentina pierde contra croacia en su segundo partido del mundial 2018">
			<p>
				Messi no apareció con la magia que lo caracteriza
			</p>
			<p>
				<strong>Es mucho el peso que lleva Messi sobre sus hombros.</strong>
				<ul>	
					<li>¿Es culpa del director técnico?</li>
					<li>¿La selección de Argentina no hace brillar a Messi?</li>
					<li>¿Messi necesita ganar la copa del mundo para culminar su carrera?</li>
				</ul>
			</p>
			<p class="meta">
				<span>Por: Armando Mariscal</span>
				<br>
				<time datetime="2018-06-21 20:00">21 de Junio del 2018 </time>
			</p>
		</div>
	</article>
	<article class="post">
		<div class="post-preview">
			<h2><a href="#">Publicidad</a></h2>
			<style type="text/css">
				.adslot_1 { display:inline-block; width: 320px; height: 50px; }
				@media (max-width: 400px) { .adslot_1 { display: none; } }
				@media (min-width:500px) { .adslot_1 { width: 468px; height: 60px; } }
				@media (min-width:800px) { .adslot_1 { width: 728px; height: 90px; } }
			</style>
			<ins class="adsbygoogle adslot_1" data-ad-client="ca-pub-9467923145415009" data-ad-slot="3118449251"></ins>
			<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
		</div>
	</article>
</div>

<?php
	include($path . 'v/footer.phtml');
?>