<?php
	$path = '../../../';
	$description = 'Inician Las Rebajas de Verano 2018 De Steam';
	$keywords = 'Inician Las Rebajas de Verano 2018 De Steam';
	include($path . 'v/head.phtml');
	include($path . 'v/menu.phtml');
?>
<div class="posts-block animated fadeIn ">
	<article class="post">
		<div class="post-preview">
			<h2>Inician Las Rebajas de Verano 2018 De Steam</h2>
			<h3>Saliens ¡Saliens lucha contra los duldrumz!</h3>
			<img src="img/inician-las-rebajas-de-verano-2018-de-steam.png" alt="Inician Las Rebajas De Verano 2018 De Steam">
			<p>
				El día de hoy 21 de Junio y hasta el 5 de Julio del 2018 estarán las ofertas de verano de Steam.
			</p>
			<p>
				<a href="https://store.steampowered.com/">Ir a Steam</a>
			</p>
			<h3>Mis recomendaciones</h3>
			<ul>
				<li><a href="https://store.steampowered.com/app/588650/Dead_Cells/">Dead Cells.</a></li>
			</ul>
			<p class="meta">
				<span>Por: Armando Mariscal</span>
				<br>
				<time datetime="2018-06-21 20:00">20 de Junio del 2018 </time>
			</p>
		</div>
	</article>
	<article class="post">
		<div class="post-preview">
			<h2><a href="#">Publicidad</a></h2>
			<style type="text/css">
				.adslot_1 { display:inline-block; width: 320px; height: 50px; }
				@media (max-width: 400px) { .adslot_1 { display: none; } }
				@media (min-width:500px) { .adslot_1 { width: 468px; height: 60px; } }
				@media (min-width:800px) { .adslot_1 { width: 728px; height: 90px; } }
			</style>
			<ins class="adsbygoogle adslot_1" data-ad-client="ca-pub-9467923145415009" data-ad-slot="3118449251"></ins>
			<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
		</div>
	</article>
</div>

<?php
	include($path . 'v/footer.phtml');
?>