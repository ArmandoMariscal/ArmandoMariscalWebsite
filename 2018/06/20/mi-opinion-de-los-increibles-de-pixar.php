<?php
	$path = '../../../';
	$description = 'Mi Opinión de los Increibles de Disney Pixar';
	$keywords = 'los Increibles, Disney Pixar';
	include($path . 'v/head.phtml');
	include($path . 'v/menu.phtml');
?>
<div class="posts-block animated fadeIn ">
	<article class="post">
		<div class="post-preview">
			<h2>Mi Opinión de los Increibles</h2>
			<h3>Los super heroes de Disney Pixar</h3>
			<img src="img/poster-de-los-increibles-de-pixar.png" alt="Poster De Los Increibles De Pixar">
			<p>
				Por fin vi la película de los Increibles y me ha gustado bastante, ahora si, ya estoy listo para ver <strong>Los Increibles 2</strong>.
			</p>
			<h3>Me gustó</h3>
			<ul>
				<li>Ver cómo sería la vida detrás de la acción para un superheroe.</li>
				<li>Los poderes ocultos de Jack-jack... ¡Épicos!</li>
				<li>La historia del villano, descubrir cómo a exterminado poco a poco a todo los super, sus planes y todo el me encantó. <strong>¡Sindrome soy tu FAN!</strong></li>
				<li>El personaje de la diseñadora muy divertido, tiene todo para ser la nueva villana en una secuela.</li>
			</ul>
			<h3>No me gustó</h3>
			<ul>
				<li>El nombre de super heroína de Violet ¡¿Cuál es?!</li>
				<li>¿Sólo una misión cómo los Increibles? la batalla contra el hombre topo se veía genial.</li>
			</ul>
			<h3>Le doy...</h3>
			<p>
				Tres de cinco estrellas, inicia demasiada lenta y pudó haber más acción.<br>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			</p>
			<p class="meta">
				<span>Por: Armando Mariscal</span>
				<br>
				<time datetime="2018-06-20 20:00">20 de Junio del 2018 </time>
			</p>
		</div>
	</article>
	<article class="post">
		<div class="post-preview">
			<h2><a href="#">Publicidad</a></h2>
			<style type="text/css">
				.adslot_1 { display:inline-block; width: 320px; height: 50px; }
				@media (max-width: 400px) { .adslot_1 { display: none; } }
				@media (min-width:500px) { .adslot_1 { width: 468px; height: 60px; } }
				@media (min-width:800px) { .adslot_1 { width: 728px; height: 90px; } }
			</style>
			<ins class="adsbygoogle adslot_1" data-ad-client="ca-pub-9467923145415009" data-ad-slot="3118449251"></ins>
			<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
		</div>
	</article>
</div>

<?php
	include($path . 'v/footer.phtml');
?>