<?php
	$path = '../../../';
	$description = 'México gana a Alemania 1 - 0 el resultado final';
	$keywords = 'La selección mexicana gana su primer partido en la copa Mundial de Rusia 2018';
	include($path . 'v/head.phtml');
	include($path . 'v/menu.phtml');
?>
<div class="posts-block animated fadeIn ">
	<article class="post">
		<div class="post-preview">
			<h2>México gana a Alemania 1 - 0 el resultado final</h2>
			<h3>La selección mexicana gana su primer partido en la copa mundial <strong>Rusia 2018</strong></h3>
			<img src="img/mexico-gana-a-alemania-en-rusia-2018-gol-de-hirving-lozano.jpeg" alt="Gol de Hirving Lozano el Chuky en Mundial de Rusia 2018">
			<p>
				<strong>Hirving Lozano</strong> <i>"El Chucky"</i> delantero de la selección mexicana fue quién anoto el gol de la victoria al minuto 35.
			</p>
			<p>
				Por parte del equipo mexicano tuvierón una participación notable todos.
				<ul>
					<li><strong>Carlos Vela</strong> quién fue vital al inicio del partido.</li>
					<li><strong>Rafa Marquéz</strong> hizo historia al ser este su quinto mundial y ser así el tercero en lograr esta hazaña.</li>
					<li><strong>Hirving Lozano</strong> por supuesto al ser quién anoto el gol de la victoria, que desde qué estaba en el club Pachuca nos ha sorprendido.</li>
					<li>Andrés Guardado, Hector Herrera, Miguel Layún y todo el equipo <strong>¡Felicidades!</strong></li>
				</ul>
			</p>
			<p>
				Sin lugar a dudas, <strong>Juan Carlos Osorio</strong> director técnico de la selección, nos a sorprendido a todo México, 
				al final del partido en el tiempo adicional, sólo veía cómo <strong>¡Alemania tenía 5 delanteros y hasta el portero!</strong> decididos a empatar!
			</p>
			<p class="meta">
				<span>Por: Armando Mariscal</span>
				<br>
				<span>17 de Junio del 2018</span>
			</p>
		</div>
	</article>
	<article class="post">
		<div class="post-preview">
			<h2><a href="#">Publicidad</a></h2>
			<style type="text/css">
				.adslot_1 { display:inline-block; width: 320px; height: 50px; }
				@media (max-width: 400px) { .adslot_1 { display: none; } }
				@media (min-width:500px) { .adslot_1 { width: 468px; height: 60px; } }
				@media (min-width:800px) { .adslot_1 { width: 728px; height: 90px; } }
			</style>
			<ins class="adsbygoogle adslot_1" data-ad-client="ca-pub-9467923145415009" data-ad-slot="3118449251"></ins>
			<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
		</div>
	</article>
</div>
<?php
	include($path . 'v/footer.phtml');
?>